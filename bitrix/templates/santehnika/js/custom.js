$(document).ready(function() {
    $('.additional_item_head').on('click', function () {
       $(this).toggleClass('additional_open');
       $(this).siblings('.additional_items').toggleClass('active');
       $(this).toggleClass('arrow_active');
    });

    $('.additional-products').on('click','.checkbox:not(.not-js)', function(){

        var $id = $(this).data("id");
        var $price = $(this).data("price");
        var $src = $(this).data("src");
        var $product_name = $(this).data("name");

        if($(this).hasClass('active-check')){
            $('#added-product-'+$id).remove();
            $('#added-product-complect-'+$id).remove();
            updatePriceAdditionalProducts();
        }
        else {
            var append_item = '<div class="kit-information__item" id="added-product-'+$id+'"><div class="kit-information__item-image to-cart"  data-price="'+$price+'" data-id="'+$id+'"><img class="img-responsive" src="'+$src+'" alt=""></div><div class="kit-information__item-delete"></div></div>';
            $('.kit-information__list').append(append_item);
            var lastItemNumber = $('.delivery-package__package-item:last-child').children('.package-item__number').text() + 1;
            console.log(lastItemNumber);
            append_item = '';
            append_item ='<div class="delivery-package__package-item" id="added-product-complect-'+$id+'">';
            append_item +='<div class="package-item__package-information">';
            append_item +='<div class="package-information__title">';
            append_item +='<div class="package-information__name">'+$product_name+'</div>';
            append_item +='<div class="package-information__price">'+$price+'<span class="package-information__rub">р.</span></div></div></div></div>';
            $('.delivery-package__info').before(append_item);
            updatePriceAdditionalProducts();
        }
        $(this).toggleClass('active-check');
    });

    $('.kit-information__list').on('click','.kit-information__item-delete',  function() {
         var $id = $(this).siblings().data('id');
         $('#product-'+$id).toggleClass('active-check');
         $(this).parent().remove();
         $('#added-product-complect-'+$id).remove();
         updatePriceAdditionalProducts();
    });
    $('.title-value').on('click', function() {
        $('.kit-information__list').toggleClass('flex-active');
    });

    function updatePriceAdditionalProducts(){
        var $summ = 0;
        var $count =0;
        $('.kit-information__item-image:not(.no-visible)').each(function(){
            $summ += $(this).data('price');
            $count++;
        });
        $('.value').text($summ + ' руб');
        $('.count-position').text($count);
        $('.package-items__number').text($count);
    }

    $('.show_all_head').on('click', function() {
        $(this).parent().removeClass('active');
       $('.hide-kit').each(function() {
           $(this).toggleClass('active');
       });
        $('.hide_all').toggleClass('active');
    });


    $('.hide_all_head').on('click', function() {
        $(this).parent().removeClass('active');
        $('.hide-kit').each(function() {
            $(this).toggleClass('active');
        });
        $('.show_all').toggleClass('active');
    });
    $('.kit-information__add2cart-button').on('click', function() {
        var $prices = [];
        var ids = [];
        console.log($('.kit-information__item-image.to-cart'));
        $('.kit-information__item-image.to-cart').each(function(index, element){
            $prices[index] = $(element).data('price');
            ids[index] = $(element).data('id');
        });
        var data='ids='+JSON.stringify(ids);
        $.ajax({
            url: '/bitrix/templates/santehnika/add2basket/add2basket.php',
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(data) {
            }
        });
        $.ajax({
            url: '/bitrix/templates/santehnika/updcountbasket/updcountbasket.php',
            type: 'POST',
            dataType: 'json',
            success: function(data) {
                console.log(data);
                $('.nums_bask').text(data);
            }
        });
    });
    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
});