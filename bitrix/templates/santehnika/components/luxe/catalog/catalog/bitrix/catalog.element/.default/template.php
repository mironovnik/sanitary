<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$templateLibrary = array('popup');
$arParams['DETAIL_PICTURE_MODE'] = 'IMG';
$currencyList = '';
if (!empty($arResult['CURRENCIES'])) {
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
    //'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
    'TEMPLATE_CLASS' => 'bx_' . $arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
    'ID' => $strMainID,
    'PICT' => $strMainID . '_pict',
    'DISCOUNT_PICT_ID' => $strMainID . '_dsc_pict',
    'STICKER_ID' => $strMainID . '_sticker',
    'BIG_SLIDER_ID' => $strMainID . '_big_slider',
    'BIG_IMG_CONT_ID' => $strMainID . '_bigimg_cont',
    'SLIDER_CONT_ID' => $strMainID . '_slider_cont',
    'SLIDER_LIST' => $strMainID . '_slider_list',
    'SLIDER_LEFT' => $strMainID . '_slider_left',
    'SLIDER_RIGHT' => $strMainID . '_slider_right',
    'OLD_PRICE' => $strMainID . '_old_price',
    'PRICE' => $strMainID . '_price',
    'DISCOUNT_PRICE' => $strMainID . '_price_discount',
    'SLIDER_CONT_OF_ID' => $strMainID . '_slider_cont_',
    'SLIDER_LIST_OF_ID' => $strMainID . '_slider_list_',
    'SLIDER_LEFT_OF_ID' => $strMainID . '_slider_left_',
    'SLIDER_RIGHT_OF_ID' => $strMainID . '_slider_right_',
    'QUANTITY' => $strMainID . '_quantity',
    'QUANTITY_DOWN' => $strMainID . '_quant_down',
    'QUANTITY_UP' => $strMainID . '_quant_up',
    'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
    'QUANTITY_LIMIT' => $strMainID . '_quant_limit',
    'BASIS_PRICE' => $strMainID . '_basis_price',
    'BUY_LINK' => $strMainID . '_buy_link',
    'ADD_BASKET_LINK' => $strMainID . '_add_basket_link',
    'BASKET_ACTIONS' => $strMainID . '_basket_actions',
    'NOT_AVAILABLE_MESS' => $strMainID . '_not_avail',
    'COMPARE_LINK' => $strMainID . '_compare_link',
    'PROP' => $strMainID . '_prop_',
    'PROP_DIV' => $strMainID . '_skudiv',
    'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
    'OFFER_GROUP' => $strMainID . '_set_group_',
    'BASKET_PROP_DIV' => $strMainID . '_basket_prop',
);
$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;
$compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));
$strTitle = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
    : $arResult['NAME']
);
$strAlt = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
    : $arResult['NAME']
);
?><H1 class="caption_h1el"><?
    echo(
    isset($arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"] != ''
        ? $arResult["IPROPERTY_VALUES"]["ELEMENT_PAGE_TITLE"]
        : $arResult["NAME"]
    ); ?></H1>
<div class="bx_item_detail <? echo $templateData['TEMPLATE_CLASS']; ?>" id="<? echo $arItemIDs['ID']; ?>">
    <table width="100%">
        <tr>
            <td align="left">
                <? $APPLICATION->IncludeComponent(
                    "bitrix:iblock.vote",
                    "stars",
                    array(
                        "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'],
                        "IBLOCK_ID" => $arParams['IBLOCK_ID'],
                        "ELEMENT_ID" => $arResult['ID'],
                        "ELEMENT_CODE" => "",
                        "MAX_VOTE" => "5",
                        "VOTE_NAMES" => array("1", "2", "3", "4", "5"),
                        "SET_STATUS_404" => "N",
                        "DISPLAY_AS_RATING" => $arParams['VOTE_DISPLAY_AS_RATING'],
                        "CACHE_TYPE" => $arParams['CACHE_TYPE'],
                        "CACHE_TIME" => $arParams['CACHE_TIME']
                    ),
                    $component,
                    array("HIDE_ICONS" => "Y")
                ); ?>
            </td>
            <?
            $sert_path = CFile::GetPath($arResult["PROPERTIES"]["SERTIFIKAT"]["VALUE"]);
            $GLOBALS['ELEMENT_SERT'] = $sert_path;
            if ($sert_path) :
                ?>
                <td>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "page_sert",
                        Array("ELEMENT_SERT" => $sert_path)
                    ); ?>
                </td>
            <? endif; ?>
            <td align="right" width="50%">
			<span class="item_buttons_counter_block">
<?
if ($arParams['DISPLAY_COMPARE']) {
    ?>
    <a href="javascript:void(0);" class="comparerbut" id="<? echo $arItemIDs['COMPARE_LINK']; ?>"><span
                class="dobdoba"><?// echo $compareBtnMessage;
            ?>Добавить к сравнению</span></a>
    <?
}
if ($showSubscribeBtn) {

}
?>
			</span>
            </td>
        </tr>
    </table>
    <?
    reset($arResult['MORE_PHOTO']);
    $arFirstPhoto = current($arResult['MORE_PHOTO']);
    $minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']);
    ?>
    <div class="bx_item_container">
        <div class="bx_lt">
            <div class="bx_item_slider" id="<? echo $arItemIDs['BIG_SLIDER_ID']; ?>">
                <div class="bx_bigimages" id="<? echo $arItemIDs['BIG_IMG_CONT_ID']; ?>">
                    <div class="bx_bigimages_imgcontainer">
                        <span class="bx_bigimages_aligner"><img id="<? echo $arItemIDs['PICT']; ?>"
                                                                src="<? echo $arFirstPhoto['SRC']; ?>"
                                                                alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>"></span>
                        <? if ($arResult["PROPERTIES"]["NEWPRODUCT"]["VALUE"]) {
                            ?>
                            <div class="bx_sticknewprod"></div><?
                        } elseif ($minPrice['DISCOUNT_DIFF_PERCENT']) {
                            ?>
                            <div class="bx_sticksaler2"></div><?
                        } elseif ($arResult["PROPERTIES"]["SALELEADER"]["VALUE"]) {
                            ?>
                            <div class="bx_sticksaler"></div><?
                        } elseif ($arResult["PROPERTIES"]["SPECIALOFFER"]["VALUE"]) {
                            ?>
                            <div class="bx_stickspec"></div><?
                        }
                        if ($minPrice['PRINT_DISCOUNT_DIFF'] && $minPrice['DISCOUNT_DIFF_PERCENT']) {
                            ?>
                            <div class="bx_stickspec_linediscount">- <?= $minPrice['PRINT_DISCOUNT_DIFF'] ?></div><?
                        } ?>

                    </div>
                </div>
                <?
                if ($arResult['SHOW_SLIDER']) {
                    if (!isset($arResult['OFFERS']) || empty($arResult['OFFERS'])) {
                        if (5 < $arResult['MORE_PHOTO_COUNT']) {
                            $strClass = 'bx_slider_conteiner full';
                            $strOneWidth = (100 / $arResult['MORE_PHOTO_COUNT']) . '%';
                            $strWidth = (20 * $arResult['MORE_PHOTO_COUNT']) . '%';
                            $strSlideStyle = '';
                        } else {
                            $strClass = 'bx_slider_conteiner';
                            $strOneWidth = '20%';
                            $strWidth = '100%';
                            $strSlideStyle = 'display: none;';
                        }
                        ?>
                        <div style="padding: 0 6px;" class="<? // echo $strClass;
                        ?>" id="<? echo $arItemIDs['SLIDER_CONT_ID']; ?>">
                            <div class="bx_slider_scroller_container">
                                <div class="bx_slide">
                                    <ul style="width: 100%" id="<? echo $arItemIDs['SLIDER_LIST']; ?>">
                                        <?
                                        foreach ($arResult['MORE_PHOTO'] as $keya => &$arOnePhoto) {
                                            ?>
                                            <li numchi=<?= ($keya + 1) ?> data-value="<? echo $arOnePhoto['ID']; ?>"
                                                style="<?
                                                if (($keya + 1) % 4 != 0):?>margin-right:10px;<? endif; ?>margin-top:10px;width:103px;height:103px; padding-top: <? echo $strOneWidth; ?>;">
                                                <span class="cnt"><span class="cnt_item"
                                                                        style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span></span>
                                            </li>
                                            <?
                                        }
                                        unset($arOnePhoto);
                                        ?>
                                    </ul>
                                </div>
                                <? /*<div class="bx_slide_left" id="<? echo $arItemIDs['SLIDER_LEFT']; ?>" style="<? echo $strSlideStyle; ?>"></div>
	<div class="bx_slide_right" id="<? echo $arItemIDs['SLIDER_RIGHT']; ?>" style="<? echo $strSlideStyle; ?>"></div>*/
                                ?>
                            </div>
                        </div>
                        <?
                        if (count($arResult['MORE_PHOTO']) <= 18):?>
                            <style>
                                #slickfanc2 .slider__item {
                                    display: inline-block;
                                    float: none;
                                }

                                #slickfanc2 .slick-track {
                                    width: 100% !important;
                                    text-align: center;
                                    transform: none !important;
                                }
                            </style>
                        <? endif; ?>
                        <?
                    } else {
                        foreach ($arResult['OFFERS'] as $key => $arOneOffer) {
                            if (!isset($arOneOffer['MORE_PHOTO_COUNT']) || 0 >= $arOneOffer['MORE_PHOTO_COUNT'])
                                continue;
                            $strVisible = ($key == $arResult['OFFERS_SELECTED'] ? '' : 'none');
                            if (5 < $arOneOffer['MORE_PHOTO_COUNT']) {
                                $strClass = 'bx_slider_conteiner full';
                                $strOneWidth = (100 / $arOneOffer['MORE_PHOTO_COUNT']) . '%';
                                $strWidth = (20 * $arOneOffer['MORE_PHOTO_COUNT']) . '%';
                                $strSlideStyle = '';
                            } else {
                                $strClass = 'bx_slider_conteiner';
                                $strOneWidth = '20%';
                                $strWidth = '100%';
                                $strSlideStyle = 'display: none;';
                            }
                            ?>
                            <div class="<? echo $strClass; ?>"
                                 id="<? echo $arItemIDs['SLIDER_CONT_OF_ID'] . $arOneOffer['ID']; ?>"
                                 style="display: <? echo $strVisible; ?>;">
                                <div class="bx_slider_scroller_container">
                                    <div class="bx_slide">
                                        <ul style="width: <? echo $strWidth; ?>;"
                                            id="<? echo $arItemIDs['SLIDER_LIST_OF_ID'] . $arOneOffer['ID']; ?>">
                                            <?
                                            foreach ($arOneOffer['MORE_PHOTO'] as &$arOnePhoto) {
                                                ?>
                                                <li data-value="<? echo $arOneOffer['ID'] . '_' . $arOnePhoto['ID']; ?>"
                                                    style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>">
                                                    <span class="cnt"><span class="cnt_item"
                                                                            style="background-image:url('<? echo $arOnePhoto['SRC']; ?>');"></span></span>
                                                </li>
                                                <?
                                            }
                                            unset($arOnePhoto);
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="bx_slide_left"
                                         id="<? echo $arItemIDs['SLIDER_LEFT_OF_ID'] . $arOneOffer['ID'] ?>"
                                         style="<? echo $strSlideStyle; ?>"
                                         data-value="<? echo $arOneOffer['ID']; ?>"></div>
                                    <div class="bx_slide_right"
                                         id="<? echo $arItemIDs['SLIDER_RIGHT_OF_ID'] . $arOneOffer['ID'] ?>"
                                         style="<? echo $strSlideStyle; ?>"
                                         data-value="<? echo $arOneOffer['ID']; ?>"></div>
                                </div>
                            </div>
                            <?
                        }
                    }
                }
                ?>
            </div>
        </div>
        <div class="bx_rt">
            <table width="100%">
                <tr>
                    <td style="padding-right:20px;" width="70%" valign="top">
                        <table class="for_main_tabinf">
                            <tr>
                                <td><b>Код товара:</b>&nbsp;</td>
                                <td><?= $arResult['ID'] ?></td>
                            </tr>
                            <? if ($arResult['PROPERTIES']["proizvodstvo"]["VALUE"]): ?>
                                <tr>
                                    <td><b>Производитель:</b>&nbsp;</td>
                                    <td><a rel="nofollow" style="text-decoration:underline;"
                                           href="/brands/proizvodstvo-is-<?= rawurlencode(urlencode(toLower($arResult['PROPERTIES']["proizvodstvo"]["VALUE"]))) ?>/"><?= $arResult['PROPERTIES']["proizvodstvo"]["VALUE"] ?></a>
                                    </td>
                                </tr>
                            <? endif; ?>
                            <? /*if($arResult['PROPERTIES']["seriya"]["VALUE"] && $arResult['PROPERTIES']["proizvodstvo"]["VALUE"]):?>
					<tr><td><b>Колекция:</b>&nbsp;</td><td><a rel="nofollow" style="text-decoration:underline;" href="/brands/proizvodstvo-is-<?=rawurlencode(urlencode(toLower($arResult['PROPERTIES']["proizvodstvo"]["VALUE"])))?>/seriya-is-<?=rawurlencode(urlencode(toLower($arResult['PROPERTIES']["seriya"]["VALUE"])))?>/"><?=$arResult['PROPERTIES']["seriya"]["VALUE"]?></a></td></tr>
				<?elseif($arResult['PROPERTIES']["seriya"]["VALUE"]):?>
					<tr><td><b>Колекция:</b>&nbsp;</td><td><a rel="nofollow" style="text-decoration:underline;" href="/brands/seriya-is-<?=rawurlencode(urlencode(toLower($arResult['PROPERTIES']["seriya"]["VALUE"])))?>/"><?=$arResult['PROPERTIES']["seriya"]["VALUE"]?></a></td></tr>
				<?endif;'*/ ?>
                            <? if ($arResult['PROPERTIES']["seriya"]["VALUE"] && $arResult['PROPERTIES']["proizvodstvo"]["VALUE"]): ?>
                                <tr>
                                    <td><b>Колекция:</b>&nbsp;</td>
                                    <td><span><?= $arResult['PROPERTIES']["seriya"]["VALUE"] ?></span></td>
                                </tr>
                            <? elseif ($arResult['PROPERTIES']["seriya"]["VALUE"]): ?>
                                <tr>
                                    <td><b>Колекция:</b>&nbsp;</td>
                                    <td><span><?= $arResult['PROPERTIES']["seriya"]["VALUE"] ?></span></td>
                                </tr>
                            <? endif; ?>
                            <? if ($arResult['PROPERTIES']["model"]["VALUE"] && $arResult['PROPERTIES']["model"]["VALUE"] != $arResult['PROPERTIES']["seriya"]["VALUE"]): ?>
                                <tr>
                                    <td><b>Модель:</b>&nbsp;</td>
                                    <td><?= $arResult['PROPERTIES']["model"]["VALUE"] ?></td>
                                </tr>
                            <? endif; ?>
                            <? if ($arResult['PROPERTIES']["strana"]["VALUE"]): ?>
                                <tr>
                                    <td><b>Страна производитель:</b>&nbsp;</td>
                                    <td><?= $arResult['PROPERTIES']["strana"]["VALUE"] ?></td>
                                </tr>
                            <? endif; ?>
                            <? if ($arResult['PROPERTIES']["garantiya"]["VALUE"]): ?>
                                <tr>
                                    <td><b>Гарантия:</b>&nbsp;</td>
                                    <td><?= $arResult['PROPERTIES']["garantiya"]["VALUE"] ?></td>
                                </tr>
                            <? endif; ?>

                        </table><? //if($arResult['PROPERTIES']["KOMPLESES"]["VALUE"])?>
                        <div class="block_haras">
                            <? if (trim($arResult['PROPERTIES']["SOSTKOMPLS"]["VALUE"]) || trim($arResult['PROPERTIES']["SOSTKOMPLS_DOP"]["VALUE"])): ?>
                                <div class="bolcktebsinf">
                                    <div onClick="$('.harblock').removeClass('seleder');$('.sosblock').removeClass('seleder');$('.harblock').addClass('seleder');$('.sostblockinner').hide();$('.harforinner').show();"
                                         class="harblock seleder">Характеристики
                                    </div>
                                    <div onClick="$('.sosblock').removeClass('seleder');$('.harblock').removeClass('seleder');$('.sosblock').addClass('seleder');$('.harforinner').hide();$('.sostblockinner').show();"
                                         class="sosblock">Состав поставки
                                    </div>
                                </div>
                            <? endif; ?>
                            <div class="harforinner">
                                <? unset($useVoteRating, $useBrands);
                                if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS']) {
                                    ?>
                                    <div class="item_info_section">
                                        <?
                                        if ($arResult['SHOW_OFFERS_PROPS']) {
                                            ?>
                                            <dl id="<? echo $arItemIDs['DISPLAY_PROP_DIV'] ?>"
                                                style="display: none;"></dl>
                                            <?
                                        }
                                        if (!empty($arResult['DISPLAY_PROPERTIES'])) {
                                            global $USER;
                                            if (!$USER->IsAdmin()) {
                                                ?>

                                                <table width="100%">
                                                    <?
                                                    foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp) {
                                                        ?>
                                                        <tr>
                                                        <td><b><? echo $arOneProp['NAME']; ?>:</b>&nbsp;</td>
                                                        <td><?
                                                            echo(
                                                            is_array($arOneProp['DISPLAY_VALUE'])
                                                                ? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                                                                : $arOneProp['DISPLAY_VALUE']
                                                            ); ?></td></tr><?
                                                    }
                                                    unset($arOneProp);
                                                    ?>

                                                </table>
                                                <?
                                            } ?>
                                            <?
                                            global $USER;
                                            if ($USER->IsAdmin()) {

                                                //Подключим классы
                                                CModule::IncludeModule("iblock");
                                                CModule::IncludeModule("sale");
                                                $IBLOCK_ID = "2";
                                                //Соберем состав комплекта
                                                $arSelectElement = Array("ID", "IBLOCK_ID", "NAME", "CATALOG_PRICE_1", "DETAIL_PICTURE", "DETAIL_PAGE_URL", "IBLOCK_SECTION_ID", "ELEMENT_CODE", "PROPERTY_ARTNUMBER");
                                                $arFilterElement = Array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $arResult["PROPERTIES"]["PRODUCTS_K"]["VALUE"], ">CATALOG_PRICE_1" => 0);
                                                $resElement = CIBlockElement::GetList(Array(), $arFilterElement, false, false, $arSelectElement);
                                                while ($obElement = $resElement->GetNext()) {
                                                    $arResult["PRODUCTS_K"][$obElement["ID"]] = $obElement;
                                                    $arResult["PRODUCTS_K"][$obElement["ID"]]["IMAGE_SRC"] = CFile::ResizeImageGet($obElement["DETAIL_PICTURE"], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                                                }
                                                //Соберем состав набора
                                                $arFilterElement = Array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $arResult["PROPERTIES"]["PRODUCTS_N"]["VALUE"], ">CATALOG_PRICE_1" => 0);
                                                $resElement = CIBlockElement::GetList(Array(), $arFilterElement, false, false, $arSelectElement);
                                                while ($obElement = $resElement->GetNext()) {
                                                    $arResult["PRODUCTS_N"][$obElement["ID"]] = $obElement;
                                                    $arResult["PRODUCTS_N"][$obElement["ID"]]["IMAGE_SRC"] = CFile::ResizeImageGet($obElement["DETAIL_PICTURE"], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                                                }
                                                //Соберем рекомендованные товары
                                                $arFilterElement = Array("IBLOCK_ID" => $IBLOCK_ID, "ID" => $arResult["PROPERTIES"]["RECOMMEND"]["VALUE"], ">CATALOG_PRICE_1" => 0);
                                                $resElement = CIBlockElement::GetList(Array(), $arFilterElement, false, false, $arSelectElement);
                                                while ($obElement = $resElement->GetNext()) {
                                                    //Собираем разделы для получения дополнительных данных
                                                    $arResult["PRODUCTS_R_EMPTY"][$obElement["ID"]] = $obElement;
                                                    $arResult["PRODUCTS_R_EMPTY"][$obElement["ID"]]["IMAGE_SRC"] = CFile::ResizeImageGet($obElement["DETAIL_PICTURE"], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_PROPORTIONAL, true);

                                                    $arResult["PRODUCTS_R_SECTION"][$obElement["IBLOCK_SECTION_ID"]] = $obElement["IBLOCK_SECTION_ID"];

                                                    $arResult["PRODUCTS_R"][$obElement["IBLOCK_SECTION_ID"]]["ELEMENTS"][$obElement["ID"]] = $obElement;
                                                    $arResult["PRODUCTS_R"][$obElement["IBLOCK_SECTION_ID"]]["ELEMENTS"][$obElement["ID"]]["IMAGE_SRC"] = CFile::ResizeImageGet($obElement["DETAIL_PICTURE"], array('width' => 80, 'height' => 80), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                                                }


                                                $arFilterSection = array("ID" => $arResult["PRODUCTS_R_SECTION"]);
                                                $obSection = CIBlockSection::GetList(array(), $arFilterSection);
                                                while ($arSection = $obSection->Fetch()) {
                                                    //Записываем имя раздела в нужны массив
                                                    $arResult["PRODUCTS_R"][$arSection["ID"]]["NAME"] = $arSection["NAME"];
                                                }
                                                ?>
                                                <div class="tabs">
                                                    <ul class="tabs__caption ">
                                                        <li class="tabs__item active">Характеристики</li>
                                                        <? if (!empty($arResult['PRODUCTS_K']) || !empty($arResult['PRODUCTS_N']) || !empty($arResult['PRODUCTS_R_EMPTY'])) { ?>
                                                        <li class="tabs__item ">Состав поставки (<span
                                                                    class="package-items__number"><?= count($arResult['PRODUCTS_K']); ?></span>)
                                                        </li>
                                                        <?}?>
                                                    </ul>
                                                    <!--Характеристики-->
                                                    <div class="tabs__content active">
                                                        <? $char_count = 1 ?>
                                                        <?
                                                        foreach ($arResult['DISPLAY_PROPERTIES'] as &$arOneProp) { ?>
                                                            <div class="content__character content__character--<?
                                                            if ($char_count % 2 == 0) echo 'odd'; else echo 'even'; ?>">
                                                                <div class="character__name"><?= $arOneProp['NAME']; ?>
                                                                    :
                                                                </div>
                                                                <div class="character__value"><? echo(
                                                                    is_array($arOneProp['DISPLAY_VALUE'])
                                                                        ? implode(' / ', $arOneProp['DISPLAY_VALUE'])
                                                                        : $arOneProp['DISPLAY_VALUE']
                                                                    ); ?></div>
                                                            </div>
                                                            <? $char_count++; ?>
                                                            <?
                                                            if ($char_count - 1 == 2) $char_count = 1; ?>
                                                            <?
                                                        } ?>
                                                        <?
                                                        unset($arOneProp); ?>
                                                    </div>
                                                    <!--Состав поставки-->
                                                <? if (!empty($arResult['PRODUCTS_K']) || !empty($arResult['PRODUCTS_N']) || !empty($arResult['PRODUCTS_R_EMPTY'])) { ?>
                                                    <div class="tabs__content  delivery-package">
                                                        <? foreach ($arResult['PRODUCTS_K'] as $arProductK) { ?>
                                                            <div class="delivery-package__package-item">
                                                                <div class="package-item__package-information">
                                                                    <div class="package-information__title">
                                                                        <div class="package-information__name"><?= $arProductK["NAME"] ?></div>
                                                                        <div class="package-information__price"><?= (int)$arProductK["CATALOG_PRICE_1"]; ?>
                                                                            <span class="package-information__rub">р.</span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <? } ?>
                                                        <div class="delivery-package__info">
                                                            Здесь будут перечислены данные о комплектующих, которые Вы
                                                            можете выбрать в конфигураторе ниже
                                                        </div>
                                                    </div>
                                                    <?}?>
                                                </div>
                                                <?
                                            } ?>
                                            <?
                                        }
                                        ?>
                                    </div>
                                    <?
                                } ?>
                                <?
                                if ('' != $arResult['PREVIEW_TEXT']) {
                                    if (
                                        'S' == $arParams['DISPLAY_PREVIEW_TEXT_MODE']
                                        || ('E' == $arParams['DISPLAY_PREVIEW_TEXT_MODE'] && '' == $arResult['DETAIL_TEXT'])
                                    ) {
                                        ?>
                                        <div class="item_info_section">
                                            <?
                                            echo('html' == $arResult['PREVIEW_TEXT_TYPE'] ? $arResult['PREVIEW_TEXT'] : '<p>' . $arResult['PREVIEW_TEXT'] . '</p>');
                                            ?>
                                        </div>
                                        <?
                                    }
                                }
                                ?>
                            </div>
                            <div style="display:none;" class="sostblockinner">
                                <? if (trim($arResult['PROPERTIES']["KOMPLESES"]["VALUE"]) && trim($arResult['PROPERTIES']["SOSTKOMPLS_DOP"]["VALUE"])): ?>
                                    <? $kompala = explode(',', str_replace('.', ',', trim($arResult['PROPERTIES']["KOMPLESES"]["VALUE"]))); ?>
                                    <? $kompaeton = explode('|', str_replace(',', '|', trim($arResult['PROPERTIES']["SOSTKOMPLS_DOP"]["VALUE"]))); ?>
                                    <? foreach ($kompala as $kea => $vaba):
                                        $dklfo .= $kompaeton[$kea]; ?>
                                        <?= str_replace('.', '.<br/>', $kompaeton[$kea]) ?>
                                        <hr/>
                                    <? endforeach; ?>
                                    <? /*if(!$dklfo):?>
							<?=trim($arResult['PROPERTIES']["SOSTKOMPLS_DOP"]["VALUE"])?>
						<?endif;*/ ?>
                                <? else: ?>
                                    <? if ($arResult['PROPERTIES']["SOSTKOMPLS"]["VALUE"] || $arResult['PROPERTIES']["SOSTKOMPLS_DOP"]["VALUE"]): ?>
                                        <? $kompler = (trim($arResult['PROPERTIES']["SOSTKOMPLS"]["VALUE"])) ? trim($arResult['PROPERTIES']["SOSTKOMPLS"]["VALUE"]) : trim($arResult['PROPERTIES']["SOSTKOMPLS_DOP"]["VALUE"]);
                                        echo str_replace('.', '.<br/>', $kompler); ?>
                                    <? endif; ?>
                                <? endif; ?>
                            </div>
                        </div>

                        <div class="item_info_section ulliis">
                            <?
                            if ('' != $arResult['DETAIL_TEXT']) {
                                ?>
                                <div class="bx_item_description">

                                    <?
                                    if ('html' == $arResult['DETAIL_TEXT_TYPE']) {
                                        echo $arResult['DETAIL_TEXT'];
                                    } else {
                                        ?><p><? echo $arResult['DETAIL_TEXT']; ?></p><?
                                    }
                                    ?>
                                </div>
                                <?
                            }
                            ?>
                        </div>
                    </td>
                    <td width="30%" valign="top">
                        <div class="item_price">
                            <?
                            $currency = $arResult["CATALOG_CURRENCY_1"];
                            $currencyRate = CCurrency::GetByID($currency)["CURRENT_BASE_RATE"];

                            $boolDiscountShow = (0 < $minPrice['DISCOUNT_DIFF']);

                            $arrPrices = CCatalogProduct::GetOptimalPrice($arResult["ID"], 1, $USER->GetUserGroupArray(), "N");
                            $currentPrice = $arrPrices["RESULT_PRICE"]["DISCOUNT_PRICE"];
                            $formattedCurrentPriceRUB = CCurrencyLang::CurrencyFormat($currentPrice, "RUB");
                            $oldPrice = $arrPrices["RESULT_PRICE"]["BASE_PRICE"];
                            $formattedOldPriceRUB = CCurrencyLang::CurrencyFormat($oldPrice, "RUB");
                            ?>
                            <? if ($oldPrice > "0.01") { ?>
                                <div class="item_current_price"
                                     id="<? echo $arItemIDs['PRICE']; ?>"><? echo $formattedCurrentPriceRUB; ?></div>
                                <div class="item_old_price" id="<? echo $arItemIDs['OLD_PRICE']; ?>"
                                     style="display: <? echo($boolDiscountShow ? '' : 'none'); ?>"><? echo($boolDiscountShow ? $formattedOldPriceRUB : ''); ?></div>
                            <? } else { ?>
                                <div class="item_current_price" id="<? echo $arItemIDs['PRICE']; ?>">По запросу</div>
                            <? } ?>
                            <? if ($arResult["PROPERTIES"]["V_NALICHII"]["VALUE"]) : ?>
                                <div class="in_stock_container"><img src="/bitrix/images/icons/in_stock_item.png"
                                                                     class="in_stock_image">В наличии
                                </div>
                            <? endif; ?>
                        </div>
                        <?
                        if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
                            $canBuy = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
                        } else {
                            $canBuy = $arResult['CAN_BUY'];
                        }
                        $buyBtnMessage = ($arParams['MESS_BTN_BUY'] != '' ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCE_CATALOG_BUY'));
                        $addToBasketBtnMessage = ($arParams['MESS_BTN_ADD_TO_BASKET'] != '' ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD'));
                        $notAvailableMessage = ($arParams['MESS_NOT_AVAILABLE'] != '' ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE'));
                        $showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
                        $showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);

                        $showSubscribeBtn = false;
                        ?>
                        <? if ($oldPrice > "0.01") { ?>
                            <div class="item_buttons vam">
			<span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>"
                  style="display: <? echo($canBuy ? '' : 'none'); ?>;">
		<?
        /*if ($showBuyBtn)
        {
        ?>
            <a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" id="<? echo $arItemIDs['BUY_LINK']; ?>"><? echo $buyBtnMessage; ?></a>
        <?
        }*/
        if ($showAddBtn) {
            ?>
            <a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart" style="font-size:29px;margin-bottom:20px;"
               id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><? echo $addToBasketBtnMessage; ?></a>
            <a href="/bitrix/ajax/oneclick.php?ID=<?= $arResult["ID"] ?>" class="bx_big one_clickan"
               style="font-size:29px;margin-top:-10px;margin-bottom:20px;width:100%;display:block;text-align:center;height: 56px;line-height: 56px;color:#000000;border:2px #d7d7d7 solid">Купить в 1 клик</a>
            <?
        }
        ?>
			</span>
                                <span id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable"
                                      style="display: <? echo(!$canBuy ? '' : 'none'); ?>;"><? echo $notAvailableMessage; ?></span>
                            </div>
                        <? } ?>
                        <?
                        unset($minPrice);
                        if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS']) && !empty($arResult['OFFERS_PROP'])) {
                            $arSkuProps = array();
                            ?>
                            <div class="item_info_section" style="padding-right:150px;"
                                 id="<? echo $arItemIDs['PROP_DIV']; ?>">
                                <?
                                foreach ($arResult['SKU_PROPS'] as &$arProp) {
                                    if (!isset($arResult['OFFERS_PROP'][$arProp['CODE']]))
                                        continue;
                                    $arSkuProps[] = array(
                                        'ID' => $arProp['ID'],
                                        'SHOW_MODE' => $arProp['SHOW_MODE'],
                                        'VALUES_COUNT' => $arProp['VALUES_COUNT']
                                    );
                                    if ('TEXT' == $arProp['SHOW_MODE']) {
                                        if (5 < $arProp['VALUES_COUNT']) {
                                            $strClass = 'bx_item_detail_size full';
                                            $strOneWidth = (100 / $arProp['VALUES_COUNT']) . '%';
                                            $strWidth = (20 * $arProp['VALUES_COUNT']) . '%';
                                            $strSlideStyle = '';
                                        } else {
                                            $strClass = 'bx_item_detail_size';
                                            $strOneWidth = '20%';
                                            $strWidth = '100%';
                                            $strSlideStyle = 'display: none;';
                                        }
                                        ?>
                                        <div class="<? echo $strClass; ?>"
                                             id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_cont">
                                            <span class="bx_item_section_name_gray"><? echo htmlspecialcharsEx($arProp['NAME']); ?></span>
                                            <div class="bx_size_scroller_container">
                                                <div class="bx_size">
                                                    <ul id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_list"
                                                        style="width: <? echo $strWidth; ?>;margin-left:0%;">
                                                        <?
                                                        foreach ($arProp['VALUES'] as $arOneValue) {
                                                            $arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
                                                            ?>
                                                            <li data-treevalue="<? echo $arProp['ID'] . '_' . $arOneValue['ID']; ?>"
                                                                data-onevalue="<? echo $arOneValue['ID']; ?>"
                                                                style="width: <? echo $strOneWidth; ?>; display: none;">
                                                                <i title="<? echo $arOneValue['NAME']; ?>"></i><span
                                                                        class="cnt"
                                                                        title="<? echo $arOneValue['NAME']; ?>"><? echo $arOneValue['NAME']; ?></span>
                                                            </li>
                                                            <?
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                                <div class="bx_slide_left" style="<? echo $strSlideStyle; ?>"
                                                     id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_left"
                                                     data-treevalue="<? echo $arProp['ID']; ?>"></div>
                                                <div class="bx_slide_right" style="<? echo $strSlideStyle; ?>"
                                                     id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_right"
                                                     data-treevalue="<? echo $arProp['ID']; ?>"></div>
                                            </div>
                                        </div>
                                        <?
                                    } elseif ('PICT' == $arProp['SHOW_MODE']) {
                                        if (5 < $arProp['VALUES_COUNT']) {
                                            $strClass = 'bx_item_detail_scu full';
                                            $strOneWidth = (100 / $arProp['VALUES_COUNT']) . '%';
                                            $strWidth = (20 * $arProp['VALUES_COUNT']) . '%';
                                            $strSlideStyle = '';
                                        } else {
                                            $strClass = 'bx_item_detail_scu';
                                            $strOneWidth = '20%';
                                            $strWidth = '100%';
                                            $strSlideStyle = 'display: none;';
                                        }
                                        ?>
                                        <div class="<? echo $strClass; ?>"
                                             id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_cont">
                                            <span class="bx_item_section_name_gray"><? echo htmlspecialcharsEx($arProp['NAME']); ?></span>
                                            <div class="bx_scu_scroller_container">
                                                <div class="bx_scu">
                                                    <ul id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_list"
                                                        style="width: <? echo $strWidth; ?>;margin-left:0%;">
                                                        <?
                                                        foreach ($arProp['VALUES'] as $arOneValue) {
                                                            $arOneValue['NAME'] = htmlspecialcharsbx($arOneValue['NAME']);
                                                            ?>
                                                            <li data-treevalue="<? echo $arProp['ID'] . '_' . $arOneValue['ID'] ?>"
                                                                data-onevalue="<? echo $arOneValue['ID']; ?>"
                                                                style="width: <? echo $strOneWidth; ?>; padding-top: <? echo $strOneWidth; ?>; display: none;">
                                                                <i title="<? echo $arOneValue['NAME']; ?>"></i>
                                                                <span class="cnt"><span class="cnt_item"
                                                                                        style="background-image:url('<? echo $arOneValue['PICT']['SRC']; ?>');"
                                                                                        title="<? echo $arOneValue['NAME']; ?>"></span></span>
                                                            </li>
                                                            <?
                                                        }
                                                        ?>
                                                    </ul>
                                                </div>
                                                <div class="bx_slide_left" style="<? echo $strSlideStyle; ?>"
                                                     id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_left"
                                                     data-treevalue="<? echo $arProp['ID']; ?>"></div>
                                                <div class="bx_slide_right" style="<? echo $strSlideStyle; ?>"
                                                     id="<? echo $arItemIDs['PROP'] . $arProp['ID']; ?>_right"
                                                     data-treevalue="<? echo $arProp['ID']; ?>"></div>
                                            </div>
                                        </div>
                                        <?
                                    }
                                }
                                unset($arProp);
                                ?>
                            </div>
                            <?
                        }
                        ?>
                        <div class="item_info_section">
                            <?

                            if ($arParams['USE_PRODUCT_QUANTITY'] == 'Y') {
                                if ($arParams['SHOW_BASIS_PRICE'] == 'Y') {
                                    $basisPriceInfo = array(
                                        '#PRICE#' => $arResult['MIN_BASIS_PRICE']['PRINT_DISCOUNT_VALUE'],
                                        '#MEASURE#' => (isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : '')
                                    );
                                    ?>
                                    <p id="<? echo $arItemIDs['BASIS_PRICE']; ?>"
                                       class="item_section_name_gray"><? echo GetMessage('CT_BCE_CATALOG_MESS_BASIS_PRICE', $basisPriceInfo); ?></p>
                                    <?
                                }
                                ?>
                                <span class="item_section_name_gray"><? echo GetMessage('CATALOG_QUANTITY'); ?></span>
                                <div class="item_buttons vam">
				<span class="item_buttons_counter_block">
					<a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb"
                       id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>">-</a>
					<input id="<? echo $arItemIDs['QUANTITY']; ?>" type="text" class="tac transparent_input"
                           value="<? echo(isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])
                               ? 1
                               : $arResult['CATALOG_MEASURE_RATIO']
                           ); ?>">
					<a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb"
                       id="<? echo $arItemIDs['QUANTITY_UP']; ?>">+</a>
					<span class="bx_cnt_desc"
                          id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"><? echo(isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : ''); ?></span>
				</span>
                                    <span class="item_buttons_counter_block"
                                          id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>"
                                          style="display: <? echo($canBuy ? '' : 'none'); ?>;">
		<?
        if ($showBuyBtn) {
            ?>
            <a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart"
               id="<? echo $arItemIDs['BUY_LINK']; ?>"><span></span><? echo $buyBtnMessage; ?></a>
            <?
        }
        if ($showAddBtn) {
            ?>
            <a href="javascript:void(0);" class="bx_big bx_bt_button bx_cart"
               id="<? echo $arItemIDs['ADD_BASKET_LINK']; ?>"><span></span><? echo $addToBasketBtnMessage; ?></a>
            <?
        }
        ?>
				</span>
                                    <span id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="bx_notavailable"
                                          style="display: <? echo(!$canBuy ? '' : 'none'); ?>;"><? echo $notAvailableMessage; ?></span>
                                    <?
                                    if ($arParams['DISPLAY_COMPARE'] || $showSubscribeBtn) {
                                        ?>

                                        <?
                                    }
                                    ?>
                                </div>
                                <?
                                if ('Y' == $arParams['SHOW_MAX_QUANTITY']) {
                                    if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
                                        ?>
                                        <p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>"
                                           style="display: none;"><? echo GetMessage('OSTATOK'); ?>: <span></span></p>
                                        <?
                                    } else {
                                        if ('Y' == $arResult['CATALOG_QUANTITY_TRACE'] && 'N' == $arResult['CATALOG_CAN_BUY_ZERO']) {
                                            ?>
                                            <p id="<? echo $arItemIDs['QUANTITY_LIMIT']; ?>"><? echo GetMessage('OSTATOK'); ?>
                                                : <span><? echo $arResult['CATALOG_QUANTITY']; ?></span></p>
                                            <?
                                        }
                                    }
                                }
                            } else {
                                ?>

                                <?
                            }
                            unset($showAddBtn, $showBuyBtn);
                            ?>
                        </div>
                        <? if ($arResult['PROPERTIES']["GARANTIA_PRICE"]["VALUE"]) : ?>
                            <div align="left" class="warranty-container">
                                <b class="orange-text">Гарантия лучшей цены 100%!</b><br/>
                                <div style="padding:5px 0 0 0;">Хотите узнать подробности?</div>
                                <div style="padding:5px 0 0 0;">Звоните: <span class="tel">8 (495) 765-70-67</span>
                                </div>
                            </div>
                        <? endif; ?>
                        <? //if($USER->isAdmin()):?>
                        <a href="/dostavka-i-oplata/" class="dostavkablock" rel="nofollow">
                            <div align="left" class="dosticosc">
                                <b>Доставка по Москве</b><br/>(в пределах МКАД)
                                <div style="padding:5px 0 0 0;">550 р.</div>
                                <div style="padding:5px 0 0 0;">Срок от 1 дня</div>
                            </div>
                            <div align="left" class="dosticosc">
                                <b>Доставка по России</b><br/>
                                <div style="padding:5px 0 0 0;">По тарифу ТК</div>
                                <div style="padding:5px 0 0 0;">Срок от 3 дней</div>
                            </div>
                            <div align="left" class="payicoso"><b>Оплата</b><br/>

                                <div>-Наличными</div>
                                <div>-Банковской картой</div>
                                <div>-Безналичный рассчет</div>

                            </div>
                        </a>
                        <? //endif;?>
                    </td>
                </tr>
            </table>

            <div class="clb"></div>
        </div>

        <div class="bx_md">
            <div class="item_info_section">
                <?
                if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
                    if ($arResult['OFFER_GROUP']) {
                        foreach ($arResult['OFFER_GROUP_VALUES'] as $offerID) {
                            ?>
                            <span id="<? echo $arItemIDs['OFFER_GROUP'] . $offerID; ?>" style="display: none;">
<?
$APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
    ".default",
    array(
        "IBLOCK_ID" => $arResult["OFFERS_IBLOCK"],
        "ELEMENT_ID" => $offerID,
        "PRICE_CODE" => $arParams["PRICE_CODE"],
        "BASKET_URL" => $arParams["BASKET_URL"],
        "OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
        "CACHE_TYPE" => $arParams["CACHE_TYPE"],
        "CACHE_TIME" => $arParams["CACHE_TIME"],
        "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
        "TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
        "CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
        "CURRENCY_ID" => $arParams["CURRENCY_ID"]
    ),
    $component,
    array("HIDE_ICONS" => "Y")
); ?><?
?>
	</span>
                            <?
                        }
                    }
                } else {
                    if ($arResult['MODULES']['catalog'] && $arResult['OFFER_GROUP']) {
                        ?><?
                        $APPLICATION->IncludeComponent("bitrix:catalog.set.constructor",
                            ".default",
                            array(
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                                "ELEMENT_ID" => $arResult["ID"],
                                "PRICE_CODE" => $arParams["PRICE_CODE"],
                                "BASKET_URL" => $arParams["BASKET_URL"],
                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                "CACHE_TIME" => $arParams["CACHE_TIME"],
                                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                                "TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME'],
                                "CONVERT_CURRENCY" => $arParams['CONVERT_CURRENCY'],
                                "CURRENCY_ID" => $arParams["CURRENCY_ID"]
                            ),
                            $component,
                            array("HIDE_ICONS" => "Y")
                        ); ?><?
                    }
                }

                /*if ($arResult['CATALOG'] && $arParams['USE_GIFTS_DETAIL'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled("sale"))
                {
                    $APPLICATION->IncludeComponent("bitrix:sale.gift.product", ".default", array(
                            'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
                            'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
                            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
                            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
                            'SUBSCRIBE_URL_TEMPLATE' => $arResult['~SUBSCRIBE_URL_TEMPLATE'],
                            'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],

                            "SHOW_DISCOUNT_PERCENT" => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
                            "SHOW_OLD_PRICE" => $arParams['GIFTS_SHOW_OLD_PRICE'],
                            "PAGE_ELEMENT_COUNT" => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
                            "LINE_ELEMENT_COUNT" => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
                            "HIDE_BLOCK_TITLE" => $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'],
                            "BLOCK_TITLE" => $arParams['GIFTS_DETAIL_BLOCK_TITLE'],
                            "TEXT_LABEL_GIFT" => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],
                            "SHOW_NAME" => $arParams['GIFTS_SHOW_NAME'],
                            "SHOW_IMAGE" => $arParams['GIFTS_SHOW_IMAGE'],
                            "MESS_BTN_BUY" => $arParams['GIFTS_MESS_BTN_BUY'],

                            "SHOW_PRODUCTS_{$arParams['IBLOCK_ID']}" => "Y",
                            "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
                            "PRODUCT_SUBSCRIPTION" => $arParams["PRODUCT_SUBSCRIPTION"],
                            "MESS_BTN_DETAIL" => $arParams["MESS_BTN_DETAIL"],
                            "MESS_BTN_SUBSCRIBE" => $arParams["MESS_BTN_SUBSCRIBE"],
                            "TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
                            "PRICE_CODE" => $arParams["PRICE_CODE"],
                            "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
                            "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                            "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                            "BASKET_URL" => $arParams["BASKET_URL"],
                            "ADD_PROPERTIES_TO_BASKET" => $arParams["ADD_PROPERTIES_TO_BASKET"],
                            "PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
                            "PARTIAL_PRODUCT_PROPERTIES" => $arParams["PARTIAL_PRODUCT_PROPERTIES"],
                            "USE_PRODUCT_QUANTITY" => 'N',
                            "OFFER_TREE_PROPS_{$arResult['OFFERS_IBLOCK']}" => $arParams['OFFER_TREE_PROPS'],
                            "CART_PROPERTIES_{$arResult['OFFERS_IBLOCK']}" => $arParams['OFFERS_CART_PROPERTIES'],
                            "PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
                            "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                            "POTENTIAL_PRODUCT_TO_BUY" => array(
                                'ID' => isset($arResult['ID']) ? $arResult['ID'] : null,
                                'MODULE' => isset($arResult['MODULE']) ? $arResult['MODULE'] : 'catalog',
                                'PRODUCT_PROVIDER_CLASS' => isset($arResult['PRODUCT_PROVIDER_CLASS']) ? $arResult['PRODUCT_PROVIDER_CLASS'] : 'CCatalogProductProvider',
                                'QUANTITY' => isset($arResult['QUANTITY']) ? $arResult['QUANTITY'] : null,
                                'IBLOCK_ID' => isset($arResult['IBLOCK_ID']) ? $arResult['IBLOCK_ID'] : null,

                                'PRIMARY_OFFER_ID' => isset($arResult['OFFERS'][0]['ID']) ? $arResult['OFFERS'][0]['ID'] : null,
                                'SECTION' => array(
                                    'ID' => isset($arResult['SECTION']['ID']) ? $arResult['SECTION']['ID'] : null,
                                    'IBLOCK_ID' => isset($arResult['SECTION']['IBLOCK_ID']) ? $arResult['SECTION']['IBLOCK_ID'] : null,
                                    'LEFT_MARGIN' => isset($arResult['SECTION']['LEFT_MARGIN']) ? $arResult['SECTION']['LEFT_MARGIN'] : null,
                                    'RIGHT_MARGIN' => isset($arResult['SECTION']['RIGHT_MARGIN']) ? $arResult['SECTION']['RIGHT_MARGIN'] : null,
                                ),
                            )
                        ), $component, array("HIDE_ICONS" => "Y"));
                }
                if ($arResult['CATALOG'] && $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled("sale"))
                {
                    $APPLICATION->IncludeComponent(
                            "bitrix:sale.gift.main.products",
                            ".default",
                            array(
                                "PAGE_ELEMENT_COUNT" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
                                "BLOCK_TITLE" => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],

                                "OFFERS_FIELD_CODE" => $arParams["OFFERS_FIELD_CODE"],
                                "OFFERS_PROPERTY_CODE" => $arParams["OFFERS_PROPERTY_CODE"],

                                "AJAX_MODE" => $arParams["AJAX_MODE"],
                                "IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
                                "IBLOCK_ID" => $arParams["IBLOCK_ID"],

                                "ELEMENT_SORT_FIELD" => 'ID',
                                "ELEMENT_SORT_ORDER" => 'DESC',
                                //"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
                                //"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
                                "FILTER_NAME" => 'searchFilter',
                                "SECTION_URL" => $arParams["SECTION_URL"],
                                "DETAIL_URL" => $arParams["DETAIL_URL"],
                                "BASKET_URL" => $arParams["BASKET_URL"],
                                "ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
                                "PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
                                "SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],

                                "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                                "CACHE_TIME" => $arParams["CACHE_TIME"],

                                "CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
                                "SET_TITLE" => $arParams["SET_TITLE"],
                                "PROPERTY_CODE" => $arParams["PROPERTY_CODE"],
                                "PRICE_CODE" => $arParams["PRICE_CODE"],
                                "USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
                                "SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

                                "PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
                                "CONVERT_CURRENCY" => $arParams["CONVERT_CURRENCY"],
                                "CURRENCY_ID" => $arParams["CURRENCY_ID"],
                                "HIDE_NOT_AVAILABLE" => $arParams["HIDE_NOT_AVAILABLE"],
                                "TEMPLATE_THEME" => (isset($arParams["TEMPLATE_THEME"]) ? $arParams["TEMPLATE_THEME"] : ""),

                                "ADD_PICT_PROP" => (isset($arParams["ADD_PICT_PROP"]) ? $arParams["ADD_PICT_PROP"] : ""),

                                "LABEL_PROP" => (isset($arParams["LABEL_PROP"]) ? $arParams["LABEL_PROP"] : ""),
                                "OFFER_ADD_PICT_PROP" => (isset($arParams["OFFER_ADD_PICT_PROP"]) ? $arParams["OFFER_ADD_PICT_PROP"] : ""),
                                "OFFER_TREE_PROPS" => (isset($arParams["OFFER_TREE_PROPS"]) ? $arParams["OFFER_TREE_PROPS"] : ""),
                                "SHOW_DISCOUNT_PERCENT" => (isset($arParams["SHOW_DISCOUNT_PERCENT"]) ? $arParams["SHOW_DISCOUNT_PERCENT"] : ""),
                                "SHOW_OLD_PRICE" => (isset($arParams["SHOW_OLD_PRICE"]) ? $arParams["SHOW_OLD_PRICE"] : ""),
                                "MESS_BTN_BUY" => (isset($arParams["MESS_BTN_BUY"]) ? $arParams["MESS_BTN_BUY"] : ""),
                                "MESS_BTN_ADD_TO_BASKET" => (isset($arParams["MESS_BTN_ADD_TO_BASKET"]) ? $arParams["MESS_BTN_ADD_TO_BASKET"] : ""),
                                "MESS_BTN_DETAIL" => (isset($arParams["MESS_BTN_DETAIL"]) ? $arParams["MESS_BTN_DETAIL"] : ""),
                                "MESS_NOT_AVAILABLE" => (isset($arParams["MESS_NOT_AVAILABLE"]) ? $arParams["MESS_NOT_AVAILABLE"] : ""),
                                'ADD_TO_BASKET_ACTION' => (isset($arParams["ADD_TO_BASKET_ACTION"]) ? $arParams["ADD_TO_BASKET_ACTION"] : ""),
                                'SHOW_CLOSE_POPUP' => (isset($arParams["SHOW_CLOSE_POPUP"]) ? $arParams["SHOW_CLOSE_POPUP"] : ""),
                                'DISPLAY_COMPARE' => (isset($arParams['DISPLAY_COMPARE']) ? $arParams['DISPLAY_COMPARE'] : ''),
                                'COMPARE_PATH' => (isset($arParams['COMPARE_PATH']) ? $arParams['COMPARE_PATH'] : ''),
                            )
                            + array(
                                'OFFER_ID' => empty($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID']) ? $arResult['ID'] : $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'],
                                'SECTION_ID' => $arResult['SECTION']['ID'],
                                'ELEMENT_ID' => $arResult['ID'],
                            ),
                            $component,
                            array("HIDE_ICONS" => "Y")
                    );
                }*/
                ?>
            </div>
        </div>
        <div class="bx_rb">

        </div>
        <div class="bx_lb">
            <div class="tac ovh">
            </div>
            <div class="tab-section-container">
                <?
                /*if ('Y' == $arParams['USE_COMMENTS'])
                {
                ?>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:catalog.comments",
                    "",
                    array(
                        "ELEMENT_ID" => $arResult['ID'],
                        "ELEMENT_CODE" => "",
                        "IBLOCK_ID" => $arParams['IBLOCK_ID'],
                        "SHOW_DEACTIVATED" => $arParams['SHOW_DEACTIVATED'],
                        "URL_TO_COMMENT" => "",
                        "WIDTH" => "",
                        "COMMENTS_COUNT" => "5",
                        "BLOG_USE" => $arParams['BLOG_USE'],
                        "FB_USE" => $arParams['FB_USE'],
                        "FB_APP_ID" => $arParams['FB_APP_ID'],
                        "VK_USE" => $arParams['VK_USE'],
                        "VK_API_ID" => $arParams['VK_API_ID'],
                        "CACHE_TYPE" => $arParams['CACHE_TYPE'],
                        "CACHE_TIME" => $arParams['CACHE_TIME'],
                        'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                        "BLOG_TITLE" => "",
                        "BLOG_URL" => $arParams['BLOG_URL'],
                        "PATH_TO_SMILE" => "",
                        "EMAIL_NOTIFY" => $arParams['BLOG_EMAIL_NOTIFY'],
                        "AJAX_POST" => "Y",
                        "SHOW_SPAM" => "Y",
                        "SHOW_RATING" => "N",
                        "FB_TITLE" => "",
                        "FB_USER_ADMIN_ID" => "",
                        "FB_COLORSCHEME" => "light",
                        "FB_ORDER_BY" => "reverse_time",
                        "VK_TITLE" => "",
                        "TEMPLATE_THEME" => $arParams['~TEMPLATE_THEME']
                    ),
                    $component,
                    array("HIDE_ICONS" => "Y")
                );?>
                <?
                }*/
                ?>
            </div>
        </div>
        <div style="clear: both;"></div>
    </div>
    <div class="clb"></div>
</div><?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
    foreach ($arResult['JS_OFFERS'] as &$arOneJS) {
        if ($arOneJS['PRICE']['DISCOUNT_VALUE'] != $arOneJS['PRICE']['VALUE']) {
            $arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'];
            $arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
        }
        $strProps = '';
        if ($arResult['SHOW_OFFERS_PROPS']) {
            if (!empty($arOneJS['DISPLAY_PROPERTIES'])) {
                foreach ($arOneJS['DISPLAY_PROPERTIES'] as $arOneProp) {
                    $strProps .= '<dt>' . $arOneProp['NAME'] . '</dt><dd>' . (
                        is_array($arOneProp['VALUE'])
                            ? implode(' / ', $arOneProp['VALUE'])
                            : $arOneProp['VALUE']
                        ) . '</dd>';
                }
            }
        }
        $arOneJS['DISPLAY_PROPERTIES'] = $strProps;
    }
    if (isset($arOneJS))
        unset($arOneJS);
    $arJSParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => true,
            'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
            'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
            'OFFER_GROUP' => $arResult['OFFER_GROUP'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
            'USE_STICKERS' => true,
        ),
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'VISUAL' => array(
            'ID' => $arItemIDs['ID'],
        ),
        'DEFAULT_PICTURE' => array(
            'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
            'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
        ),
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'NAME' => $arResult['~NAME']
        ),
        'BASKET' => array(
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'BASKET_URL' => $arParams['BASKET_URL'],
            'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        ),
        'OFFERS' => $arResult['JS_OFFERS'],
        'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
        'TREE_PROPS' => $arSkuProps
    );
    if ($arParams['DISPLAY_COMPARE']) {
        $arJSParams['COMPARE'] = array(
            'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
            'COMPARE_PATH' => $arParams['COMPARE_PATH']
        );
    }
} else {
    $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
    if ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET'] && !$emptyProductProperties) {
        ?>
        <div id="<? echo $arItemIDs['BASKET_PROP_DIV']; ?>" style="display: none;">
            <?
            if (!empty($arResult['PRODUCT_PROPERTIES_FILL'])) {
                foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propID => $propInfo) {
                    ?>
                    <input type="hidden" name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"
                           value="<? echo htmlspecialcharsbx($propInfo['ID']); ?>">
                    <?
                    if (isset($arResult['PRODUCT_PROPERTIES'][$propID]))
                        unset($arResult['PRODUCT_PROPERTIES'][$propID]);
                }
            }
            $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
            if (!$emptyProductProperties) {
                ?>
                <table>
                    <?
                    foreach ($arResult['PRODUCT_PROPERTIES'] as $propID => $propInfo) {
                        ?>
                        <tr>
                            <td><? echo $arResult['PROPERTIES'][$propID]['NAME']; ?></td>
                            <td>
                                <?
                                if (
                                    'L' == $arResult['PROPERTIES'][$propID]['PROPERTY_TYPE']
                                    && 'C' == $arResult['PROPERTIES'][$propID]['LIST_TYPE']
                                ) {
                                    foreach ($propInfo['VALUES'] as $valueID => $value) {
                                        ?><label><input type="radio"
                                                        name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"
                                                        value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"checked"' : ''); ?>><? echo $value; ?>
                                        </label><br><?
                                    }
                                } else {
                                    ?><select
                                    name="<? echo $arParams['PRODUCT_PROPS_VARIABLE']; ?>[<? echo $propID; ?>]"><?
                                    foreach ($propInfo['VALUES'] as $valueID => $value) {
                                        ?>
                                        <option
                                        value="<? echo $valueID; ?>" <? echo($valueID == $propInfo['SELECTED'] ? '"selected"' : ''); ?>><? echo $value; ?></option><?
                                    }
                                    ?></select><?
                                }
                                ?>
                            </td>
                        </tr>
                        <?
                    }
                    ?>
                </table>
                <?
            }
            ?>
        </div>
        <?
    }
    if ($arResult['MIN_PRICE']['DISCOUNT_VALUE'] != $arResult['MIN_PRICE']['VALUE']) {
        $arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'];
        $arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
    }
    $arJSParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => (isset($arResult['MIN_PRICE']) && !empty($arResult['MIN_PRICE']) && is_array($arResult['MIN_PRICE'])),
            'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
            'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
            'USE_STICKERS' => true,
        ),
        'VISUAL' => array(
            'ID' => $arItemIDs['ID'],
        ),
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'PICT' => $arFirstPhoto,
            'NAME' => $arResult['~NAME'],
            'SUBSCRIPTION' => true,
            'PRICE' => $arResult['MIN_PRICE'],
            'BASIS_PRICE' => $arResult['MIN_BASIS_PRICE'],
            'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
            'SLIDER' => $arResult['MORE_PHOTO'],
            'CAN_BUY' => $arResult['CAN_BUY'],
            'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
            'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
            'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
            'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
        ),
        'BASKET' => array(
            'ADD_PROPS' => ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y'),
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
            'EMPTY_PROPS' => $emptyProductProperties,
            'BASKET_URL' => $arParams['BASKET_URL'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        )
    );
    if ($arParams['DISPLAY_COMPARE']) {
        $arJSParams['COMPARE'] = array(
            'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
            'COMPARE_PATH' => $arParams['COMPARE_PATH']
        );
    }
    unset($emptyProductProperties);
}
?>
<script>
    fbq('track', 'ViewContent', {
        content_ids: ['<?=$arResult["ID"]?>'],
        content_type: 'product',
        value: '<? echo $arResult['MIN_PRICE']['DISCOUNT_VALUE']; ?>',
        currency: 'RUB'
    });
    $('.bx_big.bx_bt_button.bx_cart, .bx_big.one_clickan').click(function () {
        fbq('track', 'AddToCart', {
            content_ids: ['<?=$arResult["ID"]?>'],
            content_type: 'product',
            value: '<? echo $arResult['MIN_PRICE']['DISCOUNT_VALUE']; ?>',
            currency: 'RUB'
        });
    });
</script>

<script type="text/javascript">
    $('.one_clickan').fancybox({'type': 'ajax'});
    var <? echo $strObName; ?> =
    new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
    BX.message({
        ECONOMY_INFO_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO'); ?>',
        BASIS_PRICE_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_BASIS_PRICE') ?>',
        TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
        TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
        BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
        BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
        BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
        BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'); ?>',
        BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
        TITLE_SUCCESSFUL: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
        COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK') ?>',
        COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
        COMPARE_TITLE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE') ?>',
        BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
        PRODUCT_GIFT_LABEL: '<? echo GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL') ?>',
        SITE_ID: '<? echo SITE_ID; ?>'
    });
</script>

<?
global $USER;
if ($USER->IsAdmin()) { ?>
<? if (!empty($arResult['PRODUCTS_K']) || !empty($arResult['PRODUCTS_N']) || !empty($arResult['PRODUCTS_R_EMPTY'])) { ?>
    <div class="additional-products">
        <div class="kit-information">
            <div class="kit-information__head">
                <div class="kit-information__title">
                    <div class="title-head">Комплект содержит:</div>
                    <div class="title-value">
                        <span class="count-position">
                            <?
                            if (empty($arResult['PRODUCTS_K'])) {
                                echo '1';
                            } else {
                                echo count($arResult['PRODUCTS_K']);
                            }
                            ?>
                        </span>
                        позиций
                    </div>
                </div>
                <div class="kit-information__cost">
                    <div class="kit-in-cost-head">Стоимость:</div>
                    <div class="kit-in-cost-value">
                        <span class="value">
                            <?
                            if (empty($arResult['PRODUCTS_K'])) {
                                echo $formattedCurrentPriceRUB;
                            } else {
                                $complectSum = 0;
                                foreach ($arResult["PRODUCTS_K"] as $arProductK) {
                                    $complectSum += (int)$arProductK["CATALOG_PRICE_1"];
                                }
                                $complectSum += $formattedCurrentPriceRUB;
                                echo $complectSum;
                            }
                            ?>
                         руб. </span>
                    </div>
                </div>
                <div class="kit-information__add2cart">
                    <button class="kit-information__add2cart-button">В корзину</button>
                </div>
            </div>
            <div class="kit-information__list flex-active">
                    <div class="kit-information__item " style="display: none;">
                        <div class="kit-information__item-image no-visible to-cart" data-price="<?=$currentPrice?>" data-id="<?= $arResult["ID"] ?>"
                             data-added="product-<?= $arResult["ID"] ?>">
                            <img class="img-responsive" src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>"
                                 alt="<?= $arResult["NAME"] ?>" title="<?= $arResult["NAME"] ?>">
                        </div>
                    </div>
                <? foreach ($arResult["PRODUCTS_K"] as $arProductK) { ?>
                    <div class="kit-information__item">
                        <div class="kit-information__item-image" data-price="<?= (int)$arProductK["CATALOG_PRICE_1"] ?>"
                             data-id="<?= $arProductK["ID"] ?>"
                             data-added="product-<?= $arProductK["ID"] ?>">
                            <img class="img-responsive" src="<?= $arProductK["IMAGE_SRC"]["src"] ?>"
                                 alt="<?= $arProductK["NAME"] ?>" title="<?= $arProductK["NAME"] ?>">
                        </div>
                    </div>
                <? } ?>

            </div>
        </div>


            <div class="parts-kit">
                <div class="parts-kit__head">Составляющие комплекта</div>
                <? if (empty($arResult['PRODUCTS_K'])) { ?>
                    <div class="parts-kit__kit-item clearfix">
                        <div class="switch-block">
                            <div class="checkbox active-check not-js">
                                <div class="yes"></div>
                            </div>
                        </div>
                        <div class="kit-item__image">
                            <img class="img-responsive" src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>"
                                 alt="<?= $arResult["NAME"] ?>" title="<?= $arResult["NAME"] ?>">
                        </div>
                        <div class="kit-item__more-information">
                            <div class="more-information__name"><?= $arResult["NAME"] ?></div>
                            <div class="more-information__code">
                                <span class="code__name">Код товара:</span>
                                <span class="code__value"><?= $arResult["PROPERTIES"]["ARTNUMBER"]["VALUE"] ?></span>
                            </div>
                        </div>
                        <div class="kit-item__cost"><? ?> <span class="rub_normal">руб.</span></div>
                    </div>
                <? } ?>
                <? foreach ($arResult["PRODUCTS_K"] as $arProductK) { ?>
                    <div class="parts-kit__kit-item clearfix">
                        <div class="switch-block ">
                            <div class="checkbox active-check not-js" id="product-<?= $arProductK["ID"] ?>"
                                 data-id="<?= $arProductK["ID"] ?>"
                                 data-price="<?= (int)$arProductK["CATALOG_PRICE_1"] ?>"
                                 data-name="<?= $arProductK["NAME"] ?>"
                                 data-src="<?= $arProductK["IMAGE_SRC"]["src"] ?>">
                                <div class="yes"></div>
                            </div>
                        </div>
                        <div class="kit-item__image">
                            <img class="img-responsive" src="<?= $arProductK["IMAGE_SRC"]["src"] ?>"
                                 alt="<?= $arProductK["NAME"] ?>" title="<?= $arProductK["NAME"] ?>">
                        </div>
                        <div class="kit-item__more-information">
                            <div class="more-information__name"><?= $arProductK["NAME"] ?></div>
                            <? if (!empty($arProductK["PROPERTY_ARTNUMBER_VALUE"])) {
                                ?>
                                <div class="more-information__code">
                                    <span class="code__name">Код товара:</span>
                                    <span class="code__value"><?= $arProductK["PROPERTY_ARTNUMBER_VALUE"] ?></span>
                                </div>
                            <? } ?>
                        </div>
                        <div class="kit-item__cost"><?= (int)$arProductK["CATALOG_PRICE_1"] ?> <span
                                    class="rub_normal">руб.</span></div>
                    </div>
                <? } ?>
                <? foreach ($arResult["PRODUCTS_N"] as $arProductN) {
                    ?>
                    <div class="parts-kit__kit-item clearfix">
                        <div class="switch-block">
                            <div class="checkbox" id="product-<?= $arProductN["ID"] ?>"
                                 data-id="<?= $arProductN["ID"] ?>"
                                 data-price="<?= (int)$arProductN["CATALOG_PRICE_1"] ?>"
                                 data-name="<?= $arProductN["NAME"] ?>"
                                 data-src="<?= $arProductN["IMAGE_SRC"]["src"] ?>">
                                <div class="yes"></div>
                            </div>
                        </div>
                        <div class="kit-item__image">
                            <img class="img-responsive" src="<?= $arProductN["IMAGE_SRC"]["src"] ?>"
                                 alt="<?= $arProductN["NAME"] ?>" title="<?= $arProductN["NAME"] ?>">
                        </div>
                        <div class="kit-item__more-information">
                            <div class="more-information__name"><?= $arProductN["NAME"] ?></div>
                            <? if (!empty($arProductN["PROPERTY_ARTNUMBER_VALUE"])) {
                                ?>
                                <div class="more-information__code">
                                    <span class="code__name">Код товара:</span>
                                    <span class="code__value"><?= $arProductN["PROPERTY_ARTNUMBER_VALUE"] ?></span>
                                </div>
                            <? } ?>
                        </div>
                        <div class="kit-item__cost"><?= (int)$arProductN["CATALOG_PRICE_1"] ?> <span
                                    class="rub_normal">руб.</span></div>
                    </div>
                <? } ?>
            </div>
            <? if (!empty($arResult['PRODUCTS_R_EMPTY'])) { ?>
                <div class="additional">
                    <div class="additional_head">Рекомендованные товары</div>
                    <? foreach ($arResult["PRODUCTS_R"] as $arSection) {
                        ?>
                        <div class="additional_item">
                            <div class="additional_item_head arrow_active additional_open">
                                <div class="additional_item_arrow"></div>
                                <div class="additional_item_name"><?= $arSection["NAME"] ?></div>
                            </div>
                            <div class="additional_items active">
                                <?
                                    $items_count = 0;
                                    $hideFlag = false;
                                ?>
                                <? if (count($arSection["ELEMENTS"] > 1)) {
                                    $hideFlag = true;
                                }?>
                                <? foreach ($arSection["ELEMENTS"] as $arProductR) { ?>

                                    <div class="parts-kit__kit-item <? if ($items_count > 1) echo 'hide-kit' ?>  clearfix">
                                        <div class="switch-block">
                                            <div class="checkbox " id="product-<?= $arProductR["ID"] ?>"
                                                 data-id="<?= $arProductR["ID"] ?>"
                                                 data-src="<?= $arProductR["IMAGE_SRC"]["src"] ?>"
                                                 data-name="<?= $arProductR["NAME"] ?>"
                                                 data-price="<?= (int)$arProductR["CATALOG_PRICE_1"] ?>">
                                                <div class="yes "></div>
                                            </div>
                                        </div>
                                        <div class="kit-item__image">
                                            <img class="img-responsive" src="<?= $arProductR["IMAGE_SRC"]["src"] ?>"
                                                 alt="<?= $arProductR["NAME"] ?>" title="<?= $arProductR["NAME"] ?>">
                                        </div>
                                        <div class="kit-item__more-information">
                                            <div class="more-information__name"><a
                                                        href="<?= $arProductR['DETAIL_PAGE_URL']; ?>"><?= $arProductR["NAME"] ?></a>
                                            </div>
                                            <div class="more-information__code">
                                                <? if (!empty($arProductR["PROPERTY_ARTNUMBER_VALUE"])) { ?>
                                                    <span class="code__name">Код товара:</span>
                                                    <span class="code__value"><?= $arProductR["PROPERTY_ARTNUMBER_VALUE"] ?></span>
                                                <? } ?>
                                            </div>
                                        </div>
                                        <div class="kit-item__cost"><?= (int)$arProductR["CATALOG_PRICE_1"] ?><span
                                                    class="rub_normal"> руб.</span></div>
                                    </div>
                                    <?
                                    if ($hideFlag && $items_count > 1) {
                                        $hideFlag = false;
                                        $hiddenProducts = count($arProductR);
                                        echo '<div class="show_all active"><div class="show_all_head">Показать все (<span class="count_products">' . $items_count . '</span>)</div></div>';
                                    }?>
                                    <? $items_count++; ?>
                                <? } ?>
                                <div class="hide_all">
                                    <div class="hide_all_head">Свернуть</div>
                                </div>
                            </div>
                        </div>
                    <? } ?>
                </div>
            <? } ?>
    </div>
    <?}?>
<? } ?>


